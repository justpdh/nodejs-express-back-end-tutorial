
const express = require('express');
const router = express.Router();

const dummy = require('./dummy.js');


router.get('/', function (req, res, next) {
    res.json({
        rsCode: 'S00',
        rsData: dummy.userList,
        rsMsg: ''
    });
});

router.get('/:id', function (req, res) {
    const id = Number(req.params.id);
    
    if(id !== 0){
        const user = dummy.userList.filter(user => user.id === id)[0];
        if(user){
            res.json({
                rsCode: 'S00',
                rsData: user,
                rsMsg: ''
            });
        }else{
            res.json({
                rsCode: 'E00',
                rsData: {},
                rsMsg: 'No user existed'
            });
        }
    }else{
        res.status(400).json({
            rsCode: 'E00',
            rsData: {},
            rsMsg: 'No ID available'
        });
    }
});

router.delete('/:id', function(req, res){
    const id = Number(req.params.id);

    if(id !== 0){
        const userIndex = dummy.userList.findIndex(user => user.id === id);
        console.log('userIndex=' + userIndex);
        if(userIndex !== -1){
            dummy.userList.splice(userIndex, 1);
            res.json({
                rsCode: 'S00',
                rsData: {},
                rsMsg: ''
            });
        }else{
            res.json({
                rsCode: 'E00',
                rsData: {},
                rsMsg: 'No user existed'
            });
        }

    }else{
        res.status(400).json({
            rsCode: 'E00',
            rsData: {},
            rsMsg: 'No ID available'
        });
    }
});

router.post('/', function(req, res){
    console.log('req.body=');
    console.log(req.body);

    const name = req.body.name || '';
    if(!name.length){
        return res.status(400).json({
            rsCode: 'E00',
            rsData: {},
            rsMsg: 'name is required field'
        });
    }

    const nextId = dummy.userList.reduce((maxId, user) => {
        return user.id > maxId ? user.id : maxId;
    }, 0) + 1;

    const newUser = {
        id: nextId,
        name: name
    };
    dummy.userList.push(newUser);

    res.status(201).json({
        rsCode: 'S00',
        rsData: dummy.userList,
        rsMsg: ''
    });
});

module.exports = router;
